<?php

namespace App\Services\User;


interface UserContract
{
	public function addUser($request);
	
	public function login($request);

    public function updateUser($request, $id);

    public function detailUser($id);

}