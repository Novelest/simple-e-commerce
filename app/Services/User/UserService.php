<?php
namespace App\Services\User;

use App\Models\User;
use Illuminate\Support\Facades\Hash;


class UserService implements UserContract
{

    public function addUser($request)
    {

        try {
            $userDb = new User();
            $data = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ];
            $userDb->fill($data);
            $userDb->save();

            return $userDb;

        } catch (\Exception $exception) {

            return ($exception->getMessage().' '.$exception->getCode());
        }
    }

    public function updateUser($request, $id)
    {

        try {
            $userDb = User::find($id);
            $data = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ];
            $userDb->fill($data);
            $userDb->save();

            return $userDb;

        } catch (\Exception $exception) {

            return ($exception->getMessage().' '.$exception->getCode());
        }

    }

    public function detailUser($id)
    {
        try {
            $userDb = User::find($id);

            return $userDb;

        } catch (\Exception $exception) {

            return ($exception->getMessage().' '.$exception->getCode());
        }
    }

        public function login($request)
    {

        try {

        $user = User::Where('email', $request->email)->first();

        $accessToken = $user->createToken('authToken')->accessToken;

        $userDb = ['user' => $user, 'access_token' => $accessToken];
        
        return $userDb;

        } catch (\Exception $exception) {

            return ($exception->getMessage().' '.$exception->getCode());
        }
    }


}