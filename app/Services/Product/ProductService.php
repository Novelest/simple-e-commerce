<?php
namespace App\Services\Product;

use App\Models\Product;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\ProductResource;
use App\Repositories\ProductRepository;


class ProductService implements ProductContract
{
    protected $product;

    public function __construct(ProductRepository $product){
        $this->product = $product;
    }

	public function index($request){
		try {
        $productDb = $this->product->getPaginate(5);
        $productDb = ['product' => $productDb, 'message' => 'Success'];
        return $productDb;    

        } catch (\Exception $exception) {

            return ($exception->getMessage().' '.$exception->getCode());
        }
	}
    public function addProduct($request)
    {

        try {
            $productDb = new Product();
            $data = [
                'title' => $request->title,
                'description' => $request->description,
                'price' => $request->price
            ];
            $productDb->fill($data);
            $productDb->save();

            return $productDb;

        } catch (\Exception $exception) {

            return ($exception->getMessage().' '.$exception->getCode());
        }
    }


 public function updateProduct($request, $id)
    {

        try {
            // $productDb = $this->product->find($id);
            $productDb = Product::where('id',$id)->first();
            $productDb->fill($request->toArray());
            $productDb->save();

            return $productDb;

        } catch (\Exception $exception) {

            return ($exception->getMessage().' '.$exception->getCode());
        }

    }

    public function detailProduct($id)
    {
        try {
            $productDb = $this->product->find($id);

            return $productDb;

        } catch (\Exception $exception) {

            return ($exception->getMessage().' '.$exception->getCode());
        }
    }

    public function deleteProduct($id)
    {
    	try {
            $productDb = $this->product->find($id);
            $productDb->delete();

            return $productDb;

        } catch (\Exception $exception) {

            return ($exception->getMessage().' '.$exception->getCode());
        }
    }

}



   