<?php

namespace App\Services\Product;


interface ProductContract
{
	public function addProduct($request);
	
	public function index($request);

    public function updateProduct($request, $id);

    public function detailProduct($id);

    public function deleteProduct($id);

}