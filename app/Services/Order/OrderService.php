<?php 
namespace App\Services\Order;

use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\OrderResource;
use Illuminate\Support\Facades\DB;
use App\Repositories\OrderRepository;



class OrderService implements OrderContract
{

    protected $order;

    public function __construct(OrderRepository $order){
        $this->order = $order;
    }

	public function index($request){
		try {
        $orderDb = DB::table('orders')
        ->join('users', 'users.id', '=', 'orders.user_id')
        ->join('products', 'products.id', '=', 'orders.product_id')
        ->select('name', 'email', 'price', 'qty', 'total')
        ->get();


        $orderDb = ['order' => $orderDb, 'message' => 'Success'];
        return $orderDb;    

        } catch (\Exception $exception) {

            return ($exception->getMessage().' '.$exception->getCode());
        }
	}
    public function addOrder($request)
    {

        try {
            $orderDb = new Order();
            $total = $request->qty*$request->price;
            $data = [
                'user_id' => $request->user_id,
                'product_id' => $request->product_id,
                'qty' => $request->qty,
                'total' => $total
            ];
            $orderDb->fill($data);
            $orderDb->save();
            $orderDb = ['order' => $orderDb, 'message' => 'Success Adding Data'];
            return $orderDb;

        } catch (\Exception $exception) {

            return ($exception->getMessage().' '.$exception->getCode());
        }
    }


 public function updateOrder($request, $id)
    {

        try {

            $orderDb = Order::where('id',$id)->first();
            $total = $request->qty*$request->price;
            $data = [
                'user_id' => $request->user_id,
                'product_id' => $request->product_id,
                'qty' => $request->qty,
                'total' => $total
            ];
            $orderDb->fill($data);
            $orderDb->save();

            $orderDb = ['order' => $orderDb, 'message' => 'Success Updating Data'];

            return $orderDb;

        } catch (\Exception $exception) {

            return ($exception->getMessage().' '.$exception->getCode());
        }

    }

public function updateMultipleOrder($request, $id)
    {

        try {
            $user_id [] = $request->user_id;
            $total = $request->qty*$request->price;
            $data = [
                'user_id' => $request->user_id,
                'product_id' => $request->product_id,
                'qty' => $request->qty,
                'total' => $total
            ];
            $orderDb = Order::whereIn('user_id',$user_id)->update($data);
            $orderDb = ['order' => $orderDb, 'message' => 'Success Updating Multiple Data'];

            return $orderDb;

        } catch (\Exception $exception) {

            return ($exception->getMessage().' '.$exception->getCode());
        }

    }

    public function detailOrder($request, $id)
    {
        try {

        $orderDb = DB::table('orders')
        ->join('users', 'users.id', '=', 'orders.user_id')
        ->join('products', 'products.id', '=', 'orders.product_id')
        ->select('name', 'email', 'price', 'qty', 'total')
        ->where('orders.id', $id)
        ->get();
        	$orderDb = ['order' => $orderDb, 'message' => 'Detail Success'];
            return $orderDb;

        } catch (\Exception $exception) {

            return ($exception->getMessage().' '.$exception->getCode());
        }
    }

    public function deleteOrder($id)
    {
    	try {
            $user_id [] = $id;
            $orderDb = $this->order->find($id);
            $orderDb->delete();

            $orderDb = ['order' => $orderDb, 'message' => 'Deleted'];
            return $orderDb;

        } catch (\Exception $exception) {

            return ($exception->getMessage().' '.$exception->getCode());
        }
    }

     public function deleteMultipleOrder($request, $id)
    {
        try {
            $user_id [] = $request->user_id;
            print_r($user_id);
            $orderDb = Order::whereIn('user_id',$user_id)->delete();

            $orderDb = ['order' => $orderDb, 'message' => 'Deleted'];
            return $orderDb;

        } catch (\Exception $exception) {

            return ($exception->getMessage().' '.$exception->getCode());
        }
    }

}



   