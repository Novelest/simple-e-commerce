<?php

namespace App\Services\Order;


interface OrderContract
{
	public function addOrder($request);
	
	public function index($request);

    public function updateOrder($request, $id);

    public function updateMultipleOrder($request, $id);

    public function detailOrder($request, $id);

    public function deleteOrder($id);

    public function deleteMultipleOrder($request, $id);

}