<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{

    private $statusCode;

    /**
     * Create a new resource instance.
     *
     * @param  mixed  $resource
     * @return void
     */
    public function __construct($resource, $statusCode)
    {
        // Ensure you call the parent constructor
        parent::__construct($resource);
        $this->resource = $resource;

        $this->statusCode = $statusCode;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "error" => false,
            "message" => "",
            "status" => $this->statusCode,
            'data' => [
                'id' => $this->id,
                'title' => $this->title ?? "",
                'description' => $this->description ?? "",
                'price' => $this->price ?? "",
            ],
        ];
    }
}
