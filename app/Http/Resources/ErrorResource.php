<?php
/**
 * Created By Fachruzi Ramadhan
 *
 * @Filename     ErrorResource.php
 * @LastModified 1/30/20 11:35 AM.
 *
 * Copyright (c) 2020. All rights reserved.
 */

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ErrorResource extends JsonResource
{
    protected $message;
    protected $statusCode;

    /**
     * Create a new resource instance.
     *
     * @param  mixed  $resource
     * @return void
     */
    public function __construct($resource, $statusCode, $message)
    {
        // Ensure you call the parent constructor
        parent::__construct($resource);
        $this->resource = $resource;

        $this->statusCode = $statusCode;
        $this->message = $message;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "error" => true,
            "message" => $this->message,
            "status" => $this->statusCode,
            'data' => "",

        ];
    }
}