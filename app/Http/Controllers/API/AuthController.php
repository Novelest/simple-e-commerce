<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Resources\ErrorResource;
use App\Http\Resources\UserResource;
use App\Services\User\UserContract;
use App\Http\Requests\UserRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserDetailRequest;
use Illuminate\Support\Facades\Auth;


class AuthController extends Controller
{
	   
	public function register(UserRequest $request, userContract $userContract)
	{

		$userDb = $userContract->addUser($request);

		if (is_object($userDb)){
            return new UserResource($userDb, 200);
        } else {
            return new ErrorResource($userDb, 400, $userDb);
        }

	}

	public function update(UserRequest $request, UserContract $userContract) {
        $userDb = $userContract->updateUser($request, $request->user()->id);

        if (is_object($userDb)){
            return new UserResource($userDb, 200);
        } else {
            return new ErrorResource($userDb, 400, $userDb);
        }
    }

    public function show(UserDetailRequest $request, UserContract $userContract) {
        $userDb = $userContract->detailUser($request->user()->id);

        if (is_object($userDb)){
            return new UserResource($userDb, 200);
        } else {
            return new ErrorResource($userDb, 400, $userDb);
        }
    }

	public function login(LoginRequest $request, userContract $userContract)
	{

		$userDb = $userContract->login($request);

		if (is_object($userDb)){
            return new UserResource($userDb, 200);
        } else {
            return new ErrorResource($userDb, 400, $userDb);
        }
		
	}

	public function logoutApi(Request $request)
	{

    $request->user()->token()->revoke();

    return response()->json([
        'message' => 'Logout Success']);

	}
}