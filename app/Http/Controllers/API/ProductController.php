<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\product;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ErrorResource;
use App\Http\Resources\ProductDeleteResource;
use App\Http\Requests\ProductRequestIndex;
use App\Http\Requests\ProductDetailRequest;
use App\Http\Requests\ProductRequest;
use App\Services\Product\ProductContract;

class ProductController extends Controller
{
 
    public function index(ProductRequestIndex $request, ProductContract $productContract)
    {

        $productDb = $productContract->index($request);

        if (is_object($productDb)){
            return new ProductResource($productDb, 200);
        } else {
            return new ErrorResource($productDb, 400, $productDb);
        }
        
    }

    public function store(ProductRequest $request, productContract $productContract)
    {

        $productDb = $productContract->addProduct($request);

        if (is_object($productDb)){
            return new ProductResource($productDb, 200);
        } else {
            return new ErrorResource($productDb, 400, $productDb);
        }

    }

    public function update(ProductRequest $request, ProductContract $productContract) {

        $productDb = $productContract->updateProduct($request, $request->id);
        if (is_object($productDb)){
            return new ProductResource($productDb, 200);
        } else {
            return new ErrorResource($productDb, 400, $productDb);
        }
    }

    public function show(ProductDetailRequest $request, ProductContract $productContract) {
        $productDb = $productContract->detailProduct($request->id);

        if (is_object($productDb)){
            return new ProductResource($productDb, 200);
        } else {
            return new ErrorResource($productDb, 400, $productDb);
        }
    }


    public function destroy(ProductDetailRequest $request, ProductContract $productContract)
    {
        $productDb = $productContract->deleteProduct($request->id);

        if (is_object($productDb)){
            return new ProductDeleteResource($productDb, 200);
        } else {
            return new ErrorResource($productDb, 400, $productDb);
        }
    }
}
