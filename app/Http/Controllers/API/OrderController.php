<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\order;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Validator;
use App\Services\Order\OrderContract;
use App\Http\Resources\OrderResource;
use App\Http\Resources\ErrorResource;
use App\Http\Resources\ProductDeleteResource;
use App\Http\Requests\OrderRequestIndex;
use App\Http\Requests\OrderDetailRequest;
use App\Http\Requests\OrderRequest;


class OrderController extends Controller
{

    public function index(OrderRequestIndex $request, OrderContract $orderContract)
    {

        $orderDb = $orderContract->index($request);

        if (is_object($orderDb)){
            return new OrderResource($orderDb, 200);
        } else {
            return new ErrorResource($orderDb, 400, $orderDb);
        }
        
    }

    public function store(OrderRequest $request, OrderContract $orderContract)
    {

        $orderDb = $orderContract->addOrder($request);

        if (is_object($orderDb)){
            return new OrderResource($orderDb, 200);
        } else {
            return new ErrorResource($orderDb, 400, $orderDb);
        }

    }

    public function update(OrderRequest $request, OrderContract $orderContract) {

        $orderDb = $orderContract->updateOrder($request, $request->id);
        if (is_object($orderDb)){
            return new OrderResource($orderDb, 200);
        } else {
            return new ErrorResource($orderDb, 400, $orderDb);
        }
    }

    public function updateMultiple(OrderRequest $request, OrderContract $orderContract) {

        $orderDb = $orderContract->updateMultipleOrder($request, $request->id);
        if (is_object($orderDb)){
            return new OrderResource($orderDb, 200);
        } else {
            return new ErrorResource($orderDb, 400, $orderDb);
        }
    }

    public function show(OrderDetailRequest $request, OrderContract $orderContract) {
        $orderDb = $orderContract->detailOrder($request, $request->id);

        if (is_object($orderDb)){
            return new OrderResource($orderDb, 200);
        } else {
            return new ErrorResource($orderDb, 400, $orderDb);
        }
    }


    public function destroy(OrderDetailRequest $request, OrderContract $orderContract)
    {
        $orderDb = $orderContract->deleteOrder($request->id);

        if (is_object($orderDb)){
            return new ProductDeleteResource($orderDb, 200);
        } else {
            return new ErrorResource($orderDb, 400, $orderDb);
        }
    }

    public function destroyMultiple(OrderDetailRequest $request, OrderContract $orderContract)
    {
        $orderDb = $orderContract->deleteMultipleOrder($request, $request->id);

        if (is_object($orderDb)){
            return new ProductDeleteResource($orderDb, 200);
        } else {
            return new ErrorResource($orderDb, 400, $orderDb);
        }
    }
}
