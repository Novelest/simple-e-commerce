<?php
namespace App\Repositories;

use App\Models\Order;

class OrderRepository
{
	protected $order;

	public function __construct(Order $order){
		$this->order = $order;
	}

	public function getPaginate($per_page)
	{
		return $this->order->orderBy('created_at', 'DESC')->paginate($per_page);
	}

	public function find($id)
	{
		return $this->order->find($id);
	}

}