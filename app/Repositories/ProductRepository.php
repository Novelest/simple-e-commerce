<?php
namespace App\Repositories;

use App\Models\Product;

class ProductRepository
{
	protected $product;

	public function __construct(Product $product){
		$this->product = $product;
	}

	public function getPaginate($per_page)
	{
		return $this->product->orderBy('created_at', 'DESC')->paginate($per_page);
	}

	public function find($id)
	{
		return $this->product->find($id);
	}

}