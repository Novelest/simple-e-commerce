<?php

namespace App\Providers;

use App\Services\User\UserContract;
use App\Services\User\UserService;
use App\Services\Product\ProductContract;
use App\Services\Product\ProductService;
use App\Services\Order\OrderContract;
use App\Services\Order\OrderService;

use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Passport;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Passport::ignoreMigrations();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
            UserContract::class,
            UserService::class
        );
        $this->app->bind(
            ProductContract::class,
            ProductService::class
        );
        $this->app->bind(
            OrderContract::class,
            OrderService::class
        );
    }
}
