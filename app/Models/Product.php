<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Product extends Model
{
    protected $guarded=[''];

    public function user(){

        return $this->belongsToMany(User::class, 'order', 'user_id', 'product_id');
    }
}
