<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Order;
use App\Models\User;
use App\Models\Product;

class Order extends Model
{
    protected $guarded = [''];

    public function scopeOfUser($query, $id) {
        $query->join('users','orders.user_id','=','users.id')->whereIn('orders.user_id', $id);
    }
    public function scopeOfProduct($query, $id) {
        $query->join('products','orders.product_id','=','products.id')->whereIn('orders.product_id', $id);
    }

}
