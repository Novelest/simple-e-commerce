<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', 'Api\AuthController@register');
Route::post('/login', 'Api\AuthController@login');
Route::get('/logout', 'Api\AuthController@logoutApi');
		Route::group(['middleware' => ['auth:api']], function () {
Route::post('/logout', 'Api\AuthController@logoutApi');
});
// Route::apiResource('/product', 'Api\productController')->middleware('auth:api');
Route::get('/product','Api\ProductController@index')->middleware('auth:api'); 
Route::post('/product','Api\ProductController@store')->middleware('auth:api'); 
Route::get('/product/{id}','Api\ProductController@show')->middleware('auth:api'); 
Route::put('/product/{id}','Api\ProductController@update')->middleware('auth:api'); 
Route::delete('/product/{id}','Api\ProductController@destroy')->middleware('auth:api'); 

Route::get('/order','Api\OrderController@index')->middleware('auth:api'); 
Route::post('/order','Api\OrderController@store')->middleware('auth:api'); 
Route::get('/order/{id}','Api\OrderController@show')->middleware('auth:api'); 
Route::put('/order/{id}','Api\OrderController@update')->middleware('auth:api'); 
Route::put('/orderMultiple/{id}','Api\OrderController@updateMultiple')->middleware('auth:api'); 
Route::delete('/order/{id}','Api\OrderController@destroy')->middleware('auth:api'); 
Route::delete('/orderMultiple/{id}','Api\OrderController@destroyMultiple')->middleware('auth:api'); 
